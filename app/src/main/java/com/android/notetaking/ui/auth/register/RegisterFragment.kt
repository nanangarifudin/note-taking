package com.android.notetaking.ui.auth.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.android.notetaking.core.utils.*
import com.android.notetaking.databinding.FragmentRegisterBinding


class RegisterFragment : Fragment() {

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            btnLogin.setOnClickListener {

                if (edtName.text.toString().isNotEmpty() && edtEmail.text.toString().isNotEmpty() && edtPassword.text.toString().isNotEmpty() && edtPasswordConfirm.text.toString().isNotEmpty()) {
                    if (edtPassword.text.toString() == edtPasswordConfirm.text.toString()) {
                        register(edtName.text.toString(), edtEmail.text.toString(), edtPassword.text.toString())
                        showToastShort("Register Success")
                    } else showToastShort("Password not match")
                } else {
                    showToastShort("Please fill all the fields")
                }
            }
        }
    }


    private fun register(name: String, email: String, password: String){
        val pref = PrefManager(requireContext()).sharedPref
        pref.edit().apply {
            putString(NAME,name)
            putString(EMAIL,email)
            putString(PASSWORD,password)
            apply()
        }
        findNavController().navigateUp()

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}