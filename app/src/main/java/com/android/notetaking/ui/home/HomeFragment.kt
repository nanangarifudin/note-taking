package com.android.notetaking.ui.home

import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.notetaking.R
import com.android.notetaking.core.data.source.local.entity.Note
import com.android.notetaking.core.data.source.local.presistance.NoteDatabase
import com.android.notetaking.core.ui.adapter.NoteAdapter
import com.android.notetaking.core.ui.dialog.AddEditDialog
import com.android.notetaking.core.ui.viewmodel.Event
import com.android.notetaking.core.ui.viewmodel.ViewModelFactory
import com.android.notetaking.core.utils.*
import com.android.notetaking.databinding.FragmentHomeBinding

class HomeFragment : Fragment(), OnNoteItemClickListener, OnEditDialog, OnAddDialog {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: HomeViewModel
    private lateinit var noteAdapter: NoteAdapter
    private lateinit var pref: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val factory = ViewModelFactory.getInstance(NoteDatabase.getInstance(requireContext()).noteDao)
        viewModel = factory.create(HomeViewModel::class.java)
        noteAdapter = NoteAdapter(this@HomeFragment)
        pref = PrefManager(requireContext()).sharedPref
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleLogout(pref)
        binding.apply {
            btnLogout.setOnClickListener {
                pref.edit().putBoolean(IS_LOGGED_IN, false).apply()
                handleLogout(pref)
            }

            fabAdd.setOnClickListener {
                val dialog = AddEditDialog(addListener = this@HomeFragment)
                dialog.show(childFragmentManager, "AddEditDialog")
            }
            rvNote.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = noteAdapter
            }
        }

        observe(viewModel.message, ::showMessage)
        observe(viewModel.getAllNotes(), ::resultAllNotes)
    }

    private fun resultAllNotes(notes: List<Note>?) {
        notes?.let { noteAdapter.submitData(notes as MutableList<Note>) }
    }

    private fun showMessage(message: Event<String>?) {
        message?.getContentIfNotHandled()?.let {
            showToastShort(it)
        }
    }

    private fun handleLogout(pref: SharedPreferences){
        val isLoggedIn = pref.getBoolean(IS_LOGGED_IN, false)
        if (!isLoggedIn) findNavController().navigate(R.id.action_homeFragment_to_loginFragment)
    }

    override fun onEditClick(note: Note) {
        val dialog = AddEditDialog(
            note = note,
            isUpdate = true,
            editListener = this@HomeFragment)
        dialog.show(childFragmentManager, "AddEditDialog")
    }

    override fun onDeleteClick(note: Note) {
        viewModel.delete(note)
    }

    override fun onEdit(note: Note) {
        viewModel.update(note)
    }

    override fun onInsert(title: String, content: String) {
        viewModel.insert(Note(null,title, content))
    }
}