package com.android.notetaking.ui.home

import androidx.lifecycle.*
import com.android.notetaking.core.data.repository.NoteRepository
import com.android.notetaking.core.data.source.local.entity.Note
import com.android.notetaking.core.ui.viewmodel.Event
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class HomeViewModel(
    private val repository: NoteRepository
) : ViewModel() {

    private val statusMessage = MutableLiveData<Event<String>>()
    val message: LiveData<Event<String>>
        get() = statusMessage

    fun insert(note: Note) = viewModelScope.launch(Dispatchers.IO) {
        val newRowId = repository.insert(note)
        if (newRowId > -1) {
            statusMessage.value = Event("Note Inserted Successfully $newRowId")
        } else {
            statusMessage.value = Event("Error Occurred")
        }
    }

    fun update(note: Note) = viewModelScope.launch(Dispatchers.IO) {
        val updatedRowId = repository.update(note)
        if (updatedRowId > -1) {
            statusMessage.value = Event("Note Updated Successfully $updatedRowId")
        } else {
            statusMessage.value = Event("Error Occurred")
        }
    }

    fun getAllNotes() = liveData {
        repository.notes.collect {
            emit(it)
        }
    }

    fun delete(note: Note) = viewModelScope.launch(Dispatchers.IO) {
        val deletedRowId = repository.delete(note)
        if (deletedRowId > -1) {
            statusMessage.value = Event("Note Deleted Successfully $deletedRowId")
        } else {
            statusMessage.value = Event("Error Occurred")
        }
    }
}