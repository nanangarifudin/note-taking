package com.android.notetaking.ui.auth.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.android.notetaking.R
import com.android.notetaking.core.utils.*
import com.android.notetaking.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            btnLogin.setOnClickListener {
                val email = edtEmail.text.toString()
                val password = edtPassword.text.toString()
                if (email.isNotEmpty() && password.isNotEmpty()) {
                    login(email, password)
                } else {
                    showToastShort("Please enter email and password")
                }
            }
            btnRegister.setOnClickListener {
                findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
            }
        }
    }

    private fun login(email: String, password: String) {
        val pref = PrefManager(requireContext()).sharedPref
        val emailPref = pref.getString(EMAIL, "")
        val passwordPref = pref.getString(PASSWORD, "")
        if (email == emailPref && password == passwordPref) {
            pref.edit().putBoolean(IS_LOGGED_IN, true).apply()
            showToastShort("Login Success")
            findNavController().navigateUp()
        } else showToastShort("Email or password is incorrect")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}