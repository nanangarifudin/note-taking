package com.android.notetaking

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.android.notetaking.NoteTakingApp.Companion.getSharedPreferences

class NoteTakingApp : Application() {
    init {
        instance = this
    }

    companion object {
        private lateinit var instance: NoteTakingApp
        fun getContext(): Context = instance.applicationContext

        fun getSharedPreferences(): SharedPreferences = instance.getSharedPreferences(
            "NOTE_TAKING_PREF",
            Context.MODE_PRIVATE
        )
    }

}