package com.android.notetaking.core.data.source.local.presistance

import androidx.room.*
import com.android.notetaking.core.data.source.local.entity.Note
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteDao{

    @Query("SELECT * FROM note_data_table")
    fun getAllNotes(): Flow<List<Note>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNote(note: Note): Long

    @Update
    suspend fun updateNote(note: Note): Int

    @Delete
    suspend fun deleteNote(note: Note): Int


}