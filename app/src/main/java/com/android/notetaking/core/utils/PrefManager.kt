package com.android.notetaking.core.utils

import android.content.Context
import android.content.SharedPreferences

class PrefManager (context: Context) {

    val sharedPref: SharedPreferences = context.applicationContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
}