package com.android.notetaking.core.ui.dialog


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.android.notetaking.core.data.source.local.entity.Note
import com.android.notetaking.core.utils.OnAddDialog
import com.android.notetaking.core.utils.OnEditDialog
import com.android.notetaking.databinding.AddEditDialogBinding


class AddEditDialog(private val note: Note?=null,
                    private var isUpdate: Boolean = false,
                    private val addListener: OnAddDialog? = null,
                    private val editListener: OnEditDialog? = null)
    : DialogFragment(){
    private var _binding: AddEditDialogBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = AddEditDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            edtTitle.setText(note?.title)
            edtContent.setText(note?.content)
            btnSave.setOnClickListener {
                val title = edtTitle.text.toString()
                val content = edtContent.text.toString()
                if (isUpdate) {
                    if (title.isNotEmpty() && content.isNotEmpty()){
                        val noteUpdated = Note(note?.id, title, content)
                        editListener?.onEdit(noteUpdated)
                    }else {
                        edtTitle.error = "Title is required"
                        edtContent.error = "Content is required"
                    }

                }
                else {
                    if (title.isNotEmpty() && content.isNotEmpty()){
                        addListener?.onInsert(title, content)
                    } else {
                        edtTitle.error = "Title is required"
                        edtContent.error = "Content is required"
                    }
                }
                dismiss()
            }
        }
    }


}