package com.android.notetaking.core.di

import android.app.Application
import com.android.notetaking.core.data.repository.NoteRepository
import com.android.notetaking.core.data.source.local.presistance.NoteDao

object Injection {

    fun provideNoteRepository(dao: NoteDao) = NoteRepository.getInstance(dao)

}