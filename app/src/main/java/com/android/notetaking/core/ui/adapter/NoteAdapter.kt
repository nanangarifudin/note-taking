package com.android.notetaking.core.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.android.notetaking.core.data.source.local.entity.Note
import com.android.notetaking.core.utils.OnNoteItemClickListener
import com.android.notetaking.databinding.ItemNoteBinding

class NoteAdapter(private val listener: OnNoteItemClickListener): RecyclerView.Adapter<NoteAdapter.RecentAdapterViewHolder>() {
    inner class RecentAdapterViewHolder(val view: ItemNoteBinding) :
        RecyclerView.ViewHolder(view.root)

    private val diffCallback = object : DiffUtil.ItemCallback<Note>() {
        override fun areItemsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    fun submitData(list: MutableList<Note>) {
        differ.submitList(list) }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentAdapterViewHolder {
        val binding = ItemNoteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RecentAdapterViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecentAdapterViewHolder, position: Int) {
        holder.view.apply {
            val data = differ.currentList[position]
            txtTitle.text = data.title
            txtContent.text = data.content
            btnEdit.setOnClickListener { listener.onEditClick(data) }
            btnDelete.setOnClickListener { listener.onDeleteClick(data) }
        }
    }

    override fun getItemCount(): Int = differ.currentList.size

}