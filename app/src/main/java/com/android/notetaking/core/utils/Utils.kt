package com.android.notetaking.core.utils

import android.widget.Toast
import androidx.fragment.app.Fragment

fun Fragment.showToastShort(message: String) {
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}