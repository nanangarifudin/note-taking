package com.android.notetaking.core.utils

import com.android.notetaking.core.data.source.local.entity.Note

interface OnDialogListener {
    fun onPositiveClick()
    fun onNegativeClick()
}

interface OnNoteItemClickListener {
    fun onEditClick(note: Note)
    fun onDeleteClick(note: Note)
}

interface OnAddDialog{
    fun onInsert(title: String, content: String)
}

interface OnEditDialog{
    fun onEdit(note: Note)
}