package com.android.notetaking.core.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.notetaking.core.data.repository.NoteRepository
import com.android.notetaking.core.data.source.local.presistance.NoteDao
import com.android.notetaking.core.di.Injection
import com.android.notetaking.ui.home.HomeViewModel

class ViewModelFactory private constructor(
    private val mDataRepository: NoteRepository?,
) : ViewModelProvider.NewInstanceFactory() {

    companion object {
        @Volatile
        private var instance: ViewModelFactory? = null

        fun getInstance(dao: NoteDao?): ViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: ViewModelFactory(
                    dao?.let { Injection.provideNoteRepository(it) }
                ).apply {
                    instance = this
                }
            }
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(HomeViewModel::class.java) -> {
               mDataRepository?.let { it1 -> HomeViewModel(it1) }  as T
            }
            else -> throw Throwable("Unknown ViewModel class: " + modelClass.name)
        }
    }
}