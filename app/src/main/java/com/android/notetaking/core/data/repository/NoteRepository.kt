package com.android.notetaking.core.data.repository

import com.android.notetaking.core.data.source.local.entity.Note
import com.android.notetaking.core.data.source.local.presistance.NoteDao

class NoteRepository(private val dao: NoteDao) {


    val notes = dao.getAllNotes()

    suspend fun insert(note: Note) = dao.insertNote(note)

    suspend fun update(note: Note) = dao.updateNote(note)

    suspend fun delete(note: Note) = dao.deleteNote(note)


    companion object {
        @Volatile
        private var INSTANCE: NoteRepository? = null
        fun getInstance(dao: NoteDao): NoteRepository? {
            if (INSTANCE == null) {
                synchronized(NoteRepository::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = NoteRepository(dao)
                    }
                }
            }
            return INSTANCE
        }
    }

}