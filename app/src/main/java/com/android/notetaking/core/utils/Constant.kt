package com.android.notetaking.core.utils

const val PREF_NAME = "note_taking_pref"
const val EMAIL = "email"
const val PASSWORD = "password"
const val NAME = "name"
const val IS_LOGGED_IN = "is_logged_in"